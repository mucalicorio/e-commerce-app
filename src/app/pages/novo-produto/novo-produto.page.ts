import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ProdutosService } from 'src/app/services/produtos/produtos.service';
import { ProdutoModel } from 'src/app/models/produtos.model';
import { ToastService } from 'src/app/services/toast/toast.service';

@Component({
  selector: 'app-novo-produto',
  templateUrl: './novo-produto.page.html',
  styleUrls: ['./novo-produto.page.scss'],
})
export class NovoProdutoPage implements OnInit {

  buttonDisabled = false;

  produto: ProdutoModel = { nome: '', preco: null, estoque: null };

  constructor(private navCtrl: NavController, private produtosService: ProdutosService, private toastService: ToastService) { }

  ngOnInit() {
  }

  voltar() {
    console.log('Voltar');
    this.navCtrl.navigateBack('/tabs/produtos');
  }

  cadastrar() {
    console.log('Produto: ', this.produto);

    this.produtosService.postProduto(this.produto)
      .subscribe((data) => {
        console.log('Data: ', data);
        this.buttonDisabled = true;
        this.navCtrl.navigateBack('/tabs/produtos');
      }, (err) => {
        console.error('Error: ' + err);
        this.toastService.showToast('Erro ao tentar cadastrar novo produto.');
      });
  }

}
