import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';
import { UsuarioModel } from 'src/app/models/usuarios.model';
import { ToastService } from 'src/app/services/toast/toast.service';
import { LocalStorageService } from 'src/app/services/local-storage/local-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  usuario: UsuarioModel = { nome: '', email: '', senha: '' };

  constructor(
    private navCtrl: NavController,
    private usuariosService: UsuariosService,
    private toastService: ToastService,
    private localStorageService: LocalStorageService,
    private route: Router
  ) { }

  ngOnInit() {
  }

  ionViewDidLoad() {
    this.isLogged();
  }

  login() {
    this.usuariosService.postLogin(this.usuario.email, this.usuario.senha)
      .subscribe((data) => {
        console.log('Data Login: ', data);
        this.localStorageService.setLogin(data);
        this.route.navigateByUrl('tabs/produtos');
      }, (err) => {
        console.error('Error: ' + err);
        this.toastService.showToast('Erro ao tentar realizar login.');
      });
  }

  isLogged() {
    console.log('isLogged?');

    this.localStorageService.getLogin()
      .then((user) => {
        console.log('User Data: ', user);
        if (user) {
          this.route.navigateByUrl('tabs/produtos');
        }
      });
  }

}
