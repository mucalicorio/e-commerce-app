import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';
import { ActivatedRoute } from '@angular/router';
import { PedidosService } from 'src/app/services/pedidos/pedidos.service';
import { ToastService } from 'src/app/services/toast/toast.service';

@Component({
  selector: 'app-consulta-cliente',
  templateUrl: './consulta-cliente.page.html',
  styleUrls: ['./consulta-cliente.page.scss'],
})
export class ConsultaClientePage implements OnInit {

  pedidos: any;

  nome: string;

  constructor(
    private navCtrl: NavController,
    private pedidosService: PedidosService,
    private activatedRoute: ActivatedRoute,
    private toastService: ToastService
  ) { }

  ngOnInit() {
    this.nome = this.activatedRoute.snapshot.paramMap.get('nome');
    this.getUsuario(this.nome);
  }

  getUsuario(nomeUsuario) {
    console.log(nomeUsuario);

    return this.pedidosService.getPedidosUsuario(nomeUsuario)
      .subscribe((data) => {
        console.log(data);
        this.pedidos = data;
      }, (err) => {
        this.toastService.showToast('Erro ao tentar carregar cliente.');
        console.error('Error: ' + err);
      });
  }

  voltar() {
    console.log('Voltar');
    this.navCtrl.navigateBack('/tabs/clientes');
  }

}
