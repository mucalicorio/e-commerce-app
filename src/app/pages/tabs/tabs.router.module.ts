import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'pedidos',
        children: [
          {
            path: '',
            loadChildren: '../pedidos/pedidos.module#PedidosPageModule'
          }
        ]
      },
      {
        path: 'produtos',
        children: [
          {
            path: '',
            loadChildren: '../produtos/produtos.module#ProdutosPageModule'
          }
        ]
      },
      {
        path: 'clientes',
        children: [
          {
            path: '',
            loadChildren: '../clientes/clientes.module#ClientesPageModule'
          }
        ]
      },
      {
        path: 'perfil',
        children: [
          {
            path: '',
            loadChildren: '../perfil/perfil.module#PerfilPageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/pedidos',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/produtos',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule { }
