import { Component, OnInit } from '@angular/core';
import { PedidosService } from 'src/app/services/pedidos/pedidos.service';
import { Router } from '@angular/router';
import { ToastService } from 'src/app/services/toast/toast.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.page.html',
  styleUrls: ['./pedidos.page.scss'],
})
export class PedidosPage implements OnInit {

  pedidos: any = [];

  constructor(
    private pedidosServices: PedidosService,
    private route: Router,
    private toastService: ToastService
  ) { }

  ngOnInit() {
    this.getPedidos();
  }

  getPedidos() {
    this.pedidosServices.getPedidos()
      .subscribe((data) => {
        console.log('Data: ', data);
        this.pedidos = data;
      }, (err) => {
        this.toastService.showToast('Erro ao tentar carregar pedidos.');
        console.error('Error: ' + err);
      });
  }

  consultar(id: string) {
    this.route.navigateByUrl('consulta-pedido/' + id);
  }

}
