import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { UsuarioModel } from 'src/app/models/usuarios.model';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';
import { ToastService } from 'src/app/services/toast/toast.service';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.page.html',
  styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {

  usuario: UsuarioModel = { nome: '', email: '', senha: '' };

  buttonDisabled = false;

  constructor(private navCtrl: NavController, private usuariosService: UsuariosService, private toastService: ToastService) { }

  ngOnInit() {
  }

  confirmar() {
    this.usuariosService.postUsuario(this.usuario)
      .subscribe((data) => {
        console.log('Data: ', data);
        this.navCtrl.navigateRoot('tabs/produtos');
      }, (err) => {
        console.error('Error: ', err);
        this.toastService.showToast('Erro ao tentar realizar cadastro.');
      });
  }

  voltar() {
    this.navCtrl.navigateRoot('login');
  }

}
