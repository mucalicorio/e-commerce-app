import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { PedidosModel } from 'src/app/models/pedidos.model';
import { ProdutosService } from 'src/app/services/produtos/produtos.service';
import { PedidosService } from 'src/app/services/pedidos/pedidos.service';
import { ToastService } from 'src/app/services/toast/toast.service';
import { LocalStorageService } from 'src/app/services/local-storage/local-storage.service';

@Component({
  selector: 'app-novo-pedido',
  templateUrl: './novo-pedido.page.html',
  styleUrls: ['./novo-pedido.page.scss'],
})
export class NovoPedidoPage implements OnInit {

  buttonDisabled = false;

  produtos: any;

  pedido: PedidosModel = { status: true, quantidade: null, preco: null, produto: '', usuario: '' };

  constructor(
    private navCtrl: NavController,
    private produtosService: ProdutosService,
    private pedidosService: PedidosService,
    private toastService: ToastService,
    private localStorage: LocalStorageService
  ) { }

  ngOnInit() {
    this.getProdutos();

    this.localStorage.getLogin()
      .then((data) => {
        this.pedido.usuario = data.nome;
        console.log(this.pedido.usuario);
      })
      .catch((err) => {
        console.log('Error: ', err);
      });
  }

  getProdutos() {
    this.produtosService.getProdutos()
      .subscribe((data) => {
        console.log('Data: ', data);
        this.produtos = data;
      }, (err) => {
        this.toastService.showToast('Erro ao tentar carregar produtos.');
        console.error('Error: ' + err);
      });
  }

  voltar() {
    console.log('Voltar');
    this.navCtrl.navigateBack('/tabs/pedidos');
  }

  confirmar() {
    this.buttonDisabled = true;

    this.pedidosService.postPedido(this.pedido)
      .subscribe((data) => {
        console.log(data);
        this.navCtrl.navigateBack('/tabs/pedidos');
      }, (err) => {
        this.toastService.showToast('Erro ao tentar cadastrar novo pedido.');
        console.error('Error: ' + err);
      });
  }
}
