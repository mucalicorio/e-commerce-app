import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NovoPedidoPage } from './novo-pedido.page';
import { Location } from '@angular/common';
import { RouterTestingModule } from '@angular/router/testing';

describe('NovoPedidoPage', () => {
  let component: NovoPedidoPage;
  let fixture: ComponentFixture<NovoPedidoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NovoPedidoPage],
      providers: [
        Location,
      ],
      imports: [
        RouterTestingModule,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NovoPedidoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
