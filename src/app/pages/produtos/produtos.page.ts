import { Component, OnInit } from '@angular/core';
import { ProdutosService } from 'src/app/services/produtos/produtos.service';
import { Router } from '@angular/router';
import { ToastService } from 'src/app/services/toast/toast.service';

@Component({
  selector: 'app-produtos',
  templateUrl: './produtos.page.html',
  styleUrls: ['./produtos.page.scss'],
})
export class ProdutosPage implements OnInit {

  produtos: any = [];

  constructor(private produtosService: ProdutosService, private route: Router, private toastService: ToastService) { }

  ngOnInit() {
    this.getProdutos();
  }

  getProdutos() {
    this.produtosService.getProdutos()
      .subscribe((data) => {
        console.log('Data: ', data);
        this.produtos = data;
      }, (err) => {
        console.error('Error: ' + err);
        this.toastService.showToast('Erro ao tentar carregar os produtos.');
      });
  }

  consultar(nome: string) {
    this.route.navigateByUrl('consulta-produto/' + nome);
  }

}
