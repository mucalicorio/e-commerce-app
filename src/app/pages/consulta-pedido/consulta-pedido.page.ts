import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PedidosService } from 'src/app/services/pedidos/pedidos.service';
import { ToastService } from 'src/app/services/toast/toast.service';

@Component({
  selector: 'app-consulta-pedido',
  templateUrl: './consulta-pedido.page.html',
  styleUrls: ['./consulta-pedido.page.scss'],
})
export class ConsultaPedidoPage implements OnInit {

  pedido: any;

  id: string;

  constructor(private activatedRoute: ActivatedRoute, private pedidosService: PedidosService, private toastService: ToastService) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('idPedido');
    this.getPedido(this.id);
  }

  getPedido(id: string) {
    return this.pedidosService.getPedido(id)
      .subscribe((data) => {
        console.log(data);
        this.pedido = data;
      }, (err) => {
        this.toastService.showToast('Erro ao tentar carregar pedido.');
        console.error('Error: ' + err);
      });
  }

}
