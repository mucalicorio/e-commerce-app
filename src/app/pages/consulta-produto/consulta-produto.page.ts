import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProdutosService } from 'src/app/services/produtos/produtos.service';
import { ToastService } from 'src/app/services/toast/toast.service';

@Component({
  selector: 'app-consulta-produto',
  templateUrl: './consulta-produto.page.html',
  styleUrls: ['./consulta-produto.page.scss'],
})
export class ConsultaProdutoPage implements OnInit {

  nomeProduto: any;

  produto: any;

  constructor(private activatedRoute: ActivatedRoute, private produtosService: ProdutosService, private toastService: ToastService) { }

  ngOnInit() {
    this.nomeProduto = this.activatedRoute.snapshot.paramMap.get('nomeProduto');
    this.getProduto(this.nomeProduto);
  }

  getProduto(nome) {
    return this.produtosService.getProduto(nome)
      .subscribe((data) => {
        console.log(data);
        this.produto = data;
      }, (err) => {
        console.error('Error: ' + err);
        this.toastService.showToast('Erro ao tentar carregar o produto.');
      });
  }

}
