import { Component, OnInit } from '@angular/core';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ToastService } from 'src/app/services/toast/toast.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.page.html',
  styleUrls: ['./clientes.page.scss'],
})
export class ClientesPage implements OnInit {

  usuarios: any = [];

  constructor(
    private route: Router,
    private usuariosService: UsuariosService,
    private toastService: ToastService,
    private navCtrl: NavController
  ) { }

  ngOnInit() {
    this.getUsuarios();
  }

  getUsuarios() {
    this.usuariosService.getUsuarios()
      .subscribe((data) => {
        console.log('Data: ', data);
        this.usuarios = data;
      }, (err) => {
        this.toastService.showToast('Erro ao tentar carregar clientes.');
        console.error('Error: ', err);
      });
  }

  consultar(nome: string) {
    this.route.navigateByUrl('consulta-cliente/' + nome);
  }

  logout() {
    this.navCtrl.navigateRoot('login');
  }

}
