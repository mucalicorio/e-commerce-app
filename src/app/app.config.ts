import { InjectionToken } from '@angular/core';

export interface ApplicationConfig {
    apiUrl: string;
}

export const APP_CONFIG: ApplicationConfig = {
    apiUrl: 'http://localhost:3000/'
};
