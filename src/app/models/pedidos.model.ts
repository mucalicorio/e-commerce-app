export class PedidosModel {
    status: boolean;
    quantidade: number;
    preco: number;
    produto: string;
    usuario: string;
}
