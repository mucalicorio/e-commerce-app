import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './pages/tabs/tabs.module#TabsPageModule' },
  { path: 'pedidos', loadChildren: './pages/pedidos/pedidos.module#PedidosPageModule' },
  { path: 'produtos', loadChildren: './pages/produtos/produtos.module#ProdutosPageModule' },
  { path: 'clientes', loadChildren: './pages/clientes/clientes.module#ClientesPageModule' },
  { path: 'novo-pedido', loadChildren: './pages/novo-pedido/novo-pedido.module#NovoPedidoPageModule' },
  { path: 'novo-produto', loadChildren: './pages/novo-produto/novo-produto.module#NovoProdutoPageModule' },
  { path: 'perfil', loadChildren: './pages/perfil/perfil.module#PerfilPageModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'cadastro', loadChildren: './pages/cadastro/cadastro.module#CadastroPageModule' },
  { path: 'consulta-cliente/:nome', loadChildren: './pages/consulta-cliente/consulta-cliente.module#ConsultaClientePageModule' },
  { path: 'consulta-pedido', loadChildren: './pages/consulta-pedido/consulta-pedido.module#ConsultaPedidoPageModule' },
  { path: 'consulta-produto', loadChildren: './pages/consulta-produto/consulta-produto.module#ConsultaProdutoPageModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
