import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  private toast = null;

  constructor(private toastCtrl: ToastController) { }

  async showToast(mensagem: string) {
    if (this.toast) {
      this.toast.dismiss();
      this.toast = null;
    }
    this.toast = await this.toastCtrl.create({
      message: mensagem,
      closeButtonText: 'OK',
      showCloseButton: true,
      duration: 2500,
      position: 'bottom',
    });

    this.toast.present();
  }

  async handleError(mensagem: string, error: HttpErrorResponse) {
    this.showToast(mensagem + ' ' + error);
    console.error(mensagem + ' ' + error);
  }
}
