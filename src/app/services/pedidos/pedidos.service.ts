import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';

@Injectable({
  providedIn: 'root'
})
export class PedidosService {

  private PATH_BASE = 'pedidos';

  constructor(private apiService: ApiService) { }

  getPedidos() {
    return this.apiService.get(`${this.PATH_BASE}`);
  }

  getPedidosUsuario(nomeUsuario: string) {
    return this.apiService.get(`${this.PATH_BASE}/usuario/${nomeUsuario}`);
  }

  getPedido(id: string) {
    return this.apiService.get(`${this.PATH_BASE}/${id}`);
  }

  postPedido(body: any) {
    return this.apiService.post(`${this.PATH_BASE}`, body);
  }
}
