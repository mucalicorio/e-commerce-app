import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor(private storage: Storage) { }

  setLogin(user) {
    this.storage.set('user', user);
  }

  getLogin() {
    return this.storage.get('user');
  }
}
