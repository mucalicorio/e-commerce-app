import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';

@Injectable({
  providedIn: 'root'
})
export class ProdutosService {

  private PATH_BASE = 'produtos';

  constructor(private apiService: ApiService) { }

  getProdutos() {
    return this.apiService.get(`${this.PATH_BASE}`);
  }

  getProduto(nome: string) {
    return this.apiService.get(`${this.PATH_BASE}/${nome}`);
  }

  postProduto(body: any) {
    return this.apiService.post(`${this.PATH_BASE}`, body);
  }
}
