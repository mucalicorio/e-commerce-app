import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private URL_BASE = 'http://10.64.208.255:3000';

  constructor(private httpClient: HttpClient) { }

  getHeader() {
    return {
      headers: {
        ContentType: 'application/json'
      }
    };
  }

  get(path: string) {
    return this.httpClient.get(`${this.URL_BASE}/${path}/`, this.getHeader());
  }

  post(path: string, body: any) {
    console.log(this.getHeader());
    return this.httpClient.post(`${this.URL_BASE}/${path}/`, body, this.getHeader());
  }

  delete(path: string, body: any) {
    return this.httpClient.delete(`${this.URL_BASE}/${path}/`, { observe: body });
  }
}
