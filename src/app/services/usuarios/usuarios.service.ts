import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  private PATH_BASE = 'usuarios';

  constructor(private apiService: ApiService) { }

  getUsuarios() {
    return this.apiService.get(`${this.PATH_BASE}`);
  }

  postUsuario(usuario) {
    return this.apiService.post(`${this.PATH_BASE}`, usuario);
  }

  postLogin(emailUsuario: string, senhaUsuario: string) {
    return this.apiService.post(`${this.PATH_BASE}/login`, { email: emailUsuario, senha: senhaUsuario });
  }
}
